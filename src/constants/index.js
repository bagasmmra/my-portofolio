import {
    mobile,
    backend,
    creator,
    web,
    javascript,
    typescript,
    html,
    css,
    reactjs,
    redux,
    tailwind,
    nodejs,
    mongodb,
    git,
    figma,
    docker,
    meta,
    starbucks,
    tesla,
    shopify,
    carrent,
    jobit,
    tripguide,
    threejs,
    prodigium,
    ericsson,
    eventy,
    js,
  php
  } from "../assets";
  
  export const navLinks = [
    {
      id: "about",
      title: "About",
    },
    {
      id: "work",
      title: "Work",
    },
    {
      id: "contact",
      title: "Contact",
    },
  ];
  
  const services = [
    {
      title: "Software Engineer",
      icon: web,
    },
    // {
    //   title: "Backend Enginner",
    //   icon: mobile,
    // },
    {
      title: "Fullstack Engineer",
      icon: backend,
    },
    {
      title: "Integration Engineer",
      icon: creator,
    },
  ];
  
  const technologies = [
    // {
    //   name: "figma",
    //   icon: figma,
    // },
    {
      name: "MongoDB",
      icon: mongodb,
    },
    {
      name: "git",
      icon: git,
    },
    {
      name: "JavaScript",
      icon: javascript,
    },
    {
      name: "HTML 5",
      icon: html,
    },
    {
      name: "CSS 3",
      icon: css,
    },
    {
      name: "React JS",
      icon: reactjs,
    },
    {
      name: "Three JS",
      icon: threejs,
    },
    {
      name: "Redux Toolkit",
      icon: redux,
    },
    {
      name: "Node JS",
      icon: nodejs,
    },
    {
      name: "PHP",
      icon: php,
    },
    {
      name: "docker",
      icon: docker,
    },
    {
      name: "TypeScript",
      icon: typescript,
    },
  ];
  
  const experiences = [
    {
      title: "Software Engineer",
      company_name: "Prodigium",
      icon: prodigium,
      iconBg: "#fff",
      date: "Jun 2023 - Now",
      points: [
      // "Helped the company in setting up a Discord community of over 1k college students",
      // "Managed events and community engagement on Discord.",
      // "Invited guest speakers from top tech companies.",
      // "Developed targeted marketing assets for Flurno Programs.",
      ],
    },
    {
      title: "Software Engineer",
      company_name: "Ericsson",
      icon: ericsson,
      iconBg: "#fff",
      date: "Sep 2022 - May 2023",
      points: [
      "I played a pivotal role in release engineering, encompassing the deployment of application code. This" +
      " involved a range of tasks, from selecting and maintaining CI/CD tools to ensuring proper installation and configuration. Additionally, I conducted tests to address customer-reported issues, directed integration processes, and performed hygiene checks on integrated installations to optimize results.",
      ],
    },
    {
      title: "Web Engineer",
      company_name: "Eventy",
      icon: eventy,
      iconBg: "#fff",
      date: "Jan 2022 - Sep 2022",
      points: [
      "During my tenure as a Web Engineer, I leveraged my expertise in Laravel and Vue to develop an optimized web platform, focusing on cross-browser compatibility. I actively collaborated with designers to create visually appealing UI designs, produced design mockups, and provided client support. I also undertook website redesign projects to enhance user experiences and site interactions.",
      ],
    },
    {
      title: "Fullstack Engineer",
      company_name: "Afresto",
      icon: meta,
      iconBg: "#fff",
      date: "May 2021 - Dec 2021",
      points: [
      "I maintained complex technology infrastructure and implemented new features, utilizing technologies such as CodeIgniter, Laravel, and JavaScript. I collaborated closely with the product team to align development efforts with customer requirements, contributing ideas and suggestions in team meetings to drive project success.",
      ],
    },
    {
      title: "Fullstack Engineer",
      company_name: "PT Decalling Media Internusa",
      icon: meta,
      iconBg: "#fff",
      date: "Nov 2020 - Apr 2021",
      points: [
      "I gained valuable experience in validating third-party code, discussing project requirements with clients, and implementing changes to enhance business operations. I excelled in back-end development using PHP, ensuring the integrity and functionality of websites.",
      ],
    },
  ];
  
  const testimonials = [
    {
      testimonial:
        "Priyansh has very visible passion for his idea and it was great to witness his clarity of thoughts. He is also a good team player, ready to mould his skills if that helps him improve. These are some qualities that can go a long way in charting out a path towards success. I wish him the very best",
      name: "Khyati Bhatt",
      designation: "CEO",
      company: "Simply Body Talk",
      image: "https://media.licdn.com/dms/image/C4D03AQEpBQFei2OEtQ/profile-displayphoto-shrink_400_400/0/1650362145770?e=1694044800&v=beta&t=DQlFIbXqEC2uj_sx91baM_7e7QMpBKli03NgxYaQqbc",
    },
    {
      testimonial:
        "I highly recommend Priyansh for any future endeavors. His dedication, hard work, and passion for his work are truly inspiring. Priyansh is a rare talent, and I am confident that he will continue to make great strides in his career and achieve even greater success in the future.",
      name: "Anup Robins",
      designation: "HR Business Partner",
      company: "Experian",
      image: "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png",
    },
    {
      testimonial:
        "This guy has what it takes for the first unicorn. Just wait and watch.",
      name: "Aman Sharma",
      designation: "CTO",
      company: "Dinnerfy",
      image: "https://media.licdn.com/dms/image/D4E03AQFrs3BhOhnt5A/profile-displayphoto-shrink_400_400/0/1683687300051?e=1693440000&v=beta&t=BKh8L96K5xzfbcOMTznFCep4PPAuwTr5iIBNtiTJBEQ",
    },
  ];
  
  const projects = [
    {
      name: "Volt",
      description:
        "Volt is a comprehensive portfolio management software which offers a holistic solution for managing startup portfolios. Seamlessly manage your portfolio, track investments, and analyze performance all in one place with Volt's latest industry standards and best practices. Say goodbye to inefficiencies, embrace Volt!",
      tags: [
        {
          name: "web dev",
          color: "blue-text-gradient",
        },
        {
          name: "data visualization",
          color: "green-text-gradient",
        },
        {
          name: "software engineering",
          color: "pink-text-gradient",
        },
      ],
      image: carrent,
      source_code_link: "https://github.com/StarticField",
    },
    {
      name: "PredCo",
      description:
        "Web application that processes real-time data coming from IoT sensors installed in various industries. It serves as a powerful tool for Predictive Maintenance, enabling the detection of potential issues and triggering alerts. By optimizing scheduled maintenance activities, the application helps companies achieve significant cost savings.",
      tags: [
        {
          name: "internet of things",
          color: "blue-text-gradient",
        },
        {
          name: "elastic search",
          color: "green-text-gradient",
        },
        {
          name: "kibana",
          color: "pink-text-gradient",
        },
        {
          name: "aws-ec2",
          color: "orange-text-gradient",
        },
      ],
      image: jobit,
      source_code_link: "https://github.com/PriyanshNegi/PredCo",
    },
    {
      name: "Prompt-X",
      description:
        "Prompt-X is an undergoing project designed to train language models and enhance prompt engineering. It will offer productivity analytics, personalized recommendations and insights for Chat GPT users. With its focus on training LLM models, Prompt-X is a powerful tool for optimizing workflows and advancing natural language understanding. ",
      tags: [
        {
          name: "machine learning",
          color: "blue-text-gradient",
        },
        {
          name: "web dev",
          color: "green-text-gradient",
        },
        {
          name: "prompt engineering",
          color: "pink-text-gradient",
        },
      ],
      image: tripguide,
      source_code_link: "https://github.com/PriyanshNegi/Prompt-X-Website",
    },
  ];
  
  export { services, technologies, experiences, testimonials, projects };